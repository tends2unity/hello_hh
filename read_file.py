#!/usr/bin/env python3
import json

from cfg_file import TXT_FILE


def read_file(text_file=TXT_FILE):
    feed_json = open(text_file, mode='r+', encoding='utf-8')
    i = 1
    for line in feed_json:
        print(i, ':', json.loads(line))
        i  += 1


if __name__ == '__main__':
    read_file()
