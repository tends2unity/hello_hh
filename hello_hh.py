#!/usr/bin/env python3

import json
import urllib.request
import re
from bs4 import BeautifulSoup

import cfg_file
URL_STRING = cfg_file.URL_STRING
TXT_FILE = cfg_file.TXT_FILE


def get_html(url):
    responce = urllib.request.urlopen(url)
    return responce.read()


def parse_html(html):
    soup = BeautifulSoup(html, 'lxml')
    table = soup.find('td', class_='l-cell l-cell_search')
    rows = table.find_all('a', class_='search-result-item__name search-result-item__name_premium HH-LinkModifier')
    result_list = []
    for each in rows:
        vacancy_title = each.text
        vacancy_url = each.attrs['href']
        vacancy_id = re.match(r".+vacancy/(.+)", vacancy_url).group(1)
        result_list.append([vacancy_id, vacancy_title, vacancy_url])

    # parse next page url
    next_arrow = soup.find('a', class_='b-pager__next-text m-active-arrow HH-Pager-Controls-Next HH-Pager-Control')
    if next_arrow:
        next_url = next_arrow.attrs['href']
    else:
        next_url = None

    if next_url is None:
        pass
    elif next_url.upper().startswith('HTTPS'):
        pass
    elif next_url.upper().startswith('/SEARCH'):
        next_url = "https://hh.ru" + next_url
    else:
        exit('Unexpected URL format')

    return next_url, result_list


def main():
    global TXT_FILE
    global URL_STRING
    with open(TXT_FILE, mode='a', encoding='utf-8') as feed_json:
        while URL_STRING is not None:
            html = get_html(URL_STRING)
            URL_STRING, parsed_vacancies_list = parse_html(html)
            for each in parsed_vacancies_list:
                vacancy = {each[0]: [each[1], each[2]]}

                json.dump(vacancy, feed_json)
                feed_json.write('\n')
                print(vacancy)


if __name__ == '__main__':
    main()

